# Web control of semi-autonomous trains
Using simple httpserver and websockets for controlling pins and camera of **RPI**, video output is classified likewise **sign recognition**. Detailed description in PDF.

### Installation

```sh
$ pipenv install
```

### Debug

```sh
$ python src/.../*.py debug                 # Prints info into command line during pipeline
$ python src/.../*.py debug-visual          # Shows graphs and (transformed) images 
```

### Models
```sh
$ python src/ml/Trainer.py                  # Parent model for others, responsible for training particular model in K-Fold its evaluating (accuracy, loss, confusion) and loading or saving. Also it serves as an interface for predicting probability of classes.
```

**Two branches** are available:

#### Scikit
```sh
$ python src/ml/trainer/Shallow.py          # Defines shallow model (SVM with SGD) ~ 70% validation if HOG
$ python src/ml/trainer/Deep.py             # Defines deep model (NN with SGD) ~ 97% validation if HOG 
```

#### Tensorflow & Keras
```sh
$ python src/ml/trainer/Dense.py            # Defines dense model (categorical_crossentropy) ~ 100% validation if HOG
```

### Feature engineering                     - training and testing model pipeline (cropping + vectorizing + transforming + predicting)
```sh
$ python test/Debug.py                  # Predict particular clip from image
$ python test/Training.py               # Simulate prediction on whole image
$ python test/Configurations.py         # Collect results (time, accuracy) of possible configurations (type of learning, image size, ...) into CSV output
```

### Starting client-server application

```sh
$ python src/web/Server.py [debug]          # Starts webserver on 8080 and websockets 8989; If debug, RPI is simulated (Gear+Camera)
```

### Usage


![Client](https://i.ibb.co/G2sQpW4/client-web2.png)
![Usage](https://i.ibb.co/hm5yRGw/implementation.jpg)

